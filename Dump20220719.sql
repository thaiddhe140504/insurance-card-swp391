-- MySQL dump 10.13  Distrib 8.0.29, for Win64 (x86_64)
--
-- Host: localhost    Database: insurancecard
-- ------------------------------------------------------
-- Server version	8.0.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accidents`
--

DROP TABLE IF EXISTS `accidents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `accidents` (
  `contract_id` int NOT NULL,
  `detail` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8_general_ci DEFAULT NULL,
  `occur_time` datetime DEFAULT NULL,
  `status_id` int DEFAULT NULL,
  `resolve_time` datetime DEFAULT NULL,
  PRIMARY KEY (`contract_id`),
  KEY `accidents_status__fk` (`status_id`),
  CONSTRAINT `accidents_contracts__fk` FOREIGN KEY (`contract_id`) REFERENCES `contracts` (`contract_id`),
  CONSTRAINT `accidents_status__fk` FOREIGN KEY (`status_id`) REFERENCES `status` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accidents`
--

LOCK TABLES `accidents` WRITE;
/*!40000 ALTER TABLE `accidents` DISABLE KEYS */;
INSERT INTO `accidents` VALUES (1,'demo','2022-07-10 22:09:00',3,NULL);
/*!40000 ALTER TABLE `accidents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `accounts` (
  `account_id` int NOT NULL AUTO_INCREMENT,
  `password` varchar(64) NOT NULL,
  `username` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8_general_ci DEFAULT NULL,
  `info` int NOT NULL DEFAULT '0',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` VALUES (1,'$2a$10$NPSgUtdLQJxGD0nRCLizbe0n0NNBUrfmg6keg50rAi34MWPDkanhK','DuyThai',1,'2022-06-24 18:09:09'),(2,'$2a$10$In51vQEwMYiztPJWWUK.Ne9XPmSGEcFfko/Wq9/uvLzycJTehPm3y','User01',1,'2022-06-24 18:09:09'),(3,'$2a$10$NPSgUtdLQJxGD0nRCLizbe0n0NNBUrfmg6keg50rAi34MWPDkanhK','User02',1,'2022-06-24 18:09:09'),(4,'$2a$10$NPSgUtdLQJxGD0nRCLizbe0n0NNBUrfmg6keg50rAi34MWPDkanhK','TrungNT',0,'2022-06-24 18:09:09'),(11,'$2a$10$NPSgUtdLQJxGD0nRCLizbe0n0NNBUrfmg6keg50rAi34MWPDkanhK','staff03',0,'2022-06-24 18:09:09'),(12,'$2a$10$8c6R2RaGFOic3TBIbQNmh.u/eXqGI6qSN.mcsCBDvaVna/tYFoWZa','User03',1,'2022-06-24 18:09:09'),(21,'$2a$10$YWBdj2Y6JINxPr5IunV2Q.Ezfj9s4QvmXmwdcLjLPkyo8pLcqmA1C','User04',0,'2022-07-11 18:10:02'),(22,'$2a$10$bTJ8OVVPIcu4VdTf88cYvO4XmGn5cDtI5trdmauh1GolYObLgRZRe','User05',0,'2022-07-11 18:15:51'),(23,'$2a$10$FEA3e8LM.WSwHQVMUkU/lOYXwxz2IZ9w3XzltJqv/e7XlACvx7mmW','User06',1,'2022-07-11 18:16:50'),(24,'$2a$10$f9Kl4oTcwCuY50dkczMiU.azeBXDjsq1c1t7/iczRQT62c1.RQAFy','Staff05',0,'2022-07-11 18:19:49'),(25,'$2a$10$bTJ8OVVPIcu4VdTf88cYvO4XmGn5cDtI5trdmauh1GolYObLgRZRe','User07',0,'2022-07-11 18:38:22'),(26,'$2a$10$bTJ8OVVPIcu4VdTf88cYvO4XmGn5cDtI5trdmauh1GolYObLgRZRe','Staff04',0,'2022-07-11 18:39:20'),(27,'$2a$10$2hRenvI7vaMbgQGb0NHfnO5hLWFNW.ETAP5XiwD21I7ZV2TvrxIgm','User10',0,'2022-07-11 18:43:31'),(28,'$2a$10$kC9qvYLZUg7kqsMy6t8Rduk.722ByvWlI6JtD9H3x58eE3YpXZ9N6','User69',0,'2022-07-18 01:28:35'),(29,'$2a$10$Q719VbDZFKzw/5EFV1dJ2uN6ZUvj4AWoNuoNVf5XMZFyPqBDBmz/i','test01',1,'2022-07-19 14:56:08'),(30,'$2a$10$irTHLV2YYR6mL7OPNovcrOsyOnz8d0kX9lrXM0gEW0TgrbWvI1f.K','test02',1,'2022-07-19 15:10:23');
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contract_status`
--

DROP TABLE IF EXISTS `contract_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contract_status` (
  `contract_status_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`contract_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contract_status`
--

LOCK TABLES `contract_status` WRITE;
/*!40000 ALTER TABLE `contract_status` DISABLE KEYS */;
INSERT INTO `contract_status` VALUES (1,'Chờ duyệt tạo hợp đồng'),(2,'Đang hoạt động'),(3,'Chờ xác nhận làm mới'),(4,'Đã làm mới'),(5,'Chờ hủy'),(6,'Đã hủy'),(7,'Từ chối hủy'),(8,'Từ chối tạo hợp đồng'),(9,'Từ chối làm mới'),(10,'Đã kết thúc');
/*!40000 ALTER TABLE `contract_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contracts`
--

DROP TABLE IF EXISTS `contracts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contracts` (
  `contract_id` int NOT NULL AUTO_INCREMENT,
  `account_id` int DEFAULT NULL,
  `insurance_id` int DEFAULT NULL,
  `renew_time` datetime DEFAULT NULL,
  `cancel_time` datetime DEFAULT NULL,
  `price_id` int DEFAULT NULL,
  `duration_id` int DEFAULT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `contract_status_id` int DEFAULT NULL,
  PRIMARY KEY (`contract_id`),
  KEY `insuranceID` (`insurance_id`),
  KEY `user_contracts_durations__fk` (`duration_id`),
  KEY `user_contracts_prices__fk` (`price_id`),
  KEY `contracts_contract_status__fk` (`contract_status_id`),
  KEY `FK5owcp8rpls2i4wlmgdy4bs6q5` (`account_id`),
  CONSTRAINT `contracts_contract_status__fk` FOREIGN KEY (`contract_status_id`) REFERENCES `contract_status` (`contract_status_id`),
  CONSTRAINT `contracts_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`account_id`),
  CONSTRAINT `FK5owcp8rpls2i4wlmgdy4bs6q5` FOREIGN KEY (`account_id`) REFERENCES `users` (`account_id`),
  CONSTRAINT `user_contracts_durations__fk` FOREIGN KEY (`duration_id`) REFERENCES `durations` (`duration_id`),
  CONSTRAINT `user_contracts_prices__fk` FOREIGN KEY (`price_id`) REFERENCES `prices` (`price_id`),
  CONSTRAINT `usercontracts_insurances__fk` FOREIGN KEY (`insurance_id`) REFERENCES `insurances` (`insurance_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contracts`
--

LOCK TABLES `contracts` WRITE;
/*!40000 ALTER TABLE `contracts` DISABLE KEYS */;
INSERT INTO `contracts` VALUES (1,2,1,'2022-06-29 11:01:53',NULL,2,2,'2022-06-21 17:56:12',2),(4,2,3,NULL,NULL,2,1,'2022-06-27 02:07:59',2),(5,3,1,NULL,'2022-07-12 01:14:21',1,1,'2022-06-29 01:14:06',10),(6,2,1,NULL,NULL,1,1,'2022-06-29 01:24:15',1);
/*!40000 ALTER TABLE `contracts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `durations`
--

DROP TABLE IF EXISTS `durations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `durations` (
  `duration_id` int NOT NULL AUTO_INCREMENT,
  `duration` int NOT NULL,
  PRIMARY KEY (`duration_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `durations`
--

LOCK TABLES `durations` WRITE;
/*!40000 ALTER TABLE `durations` DISABLE KEYS */;
INSERT INTO `durations` VALUES (1,24),(2,36),(3,48);
/*!40000 ALTER TABLE `durations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `insurances`
--

DROP TABLE IF EXISTS `insurances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `insurances` (
  `insurance_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8_general_ci NOT NULL,
  `target` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8_general_ci NOT NULL,
  `short_detail` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8_general_ci DEFAULT NULL,
  `scope` varchar(800) CHARACTER SET utf8mb3 COLLATE utf8_general_ci DEFAULT NULL,
  `img` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`insurance_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `insurances`
--

LOCK TABLES `insurances` WRITE;
/*!40000 ALTER TABLE `insurances` DISABLE KEYS */;
INSERT INTO `insurances` VALUES (1,'Bảo hiểm tai nạn người ngồi trên xe','Lái xe và Người ngồi trên xe (gọi chung là Người được bảo hiểm).','Áp dụng cho trường hợp người ngồi trên phương tiện bị tai nạn (bao gồm người lái và người ngồi sau).','Thiệt hại thân thể đối với Người được bảo hiểm do tai nạn liên quan trực tiếp đến việc sử dụng xe.','01.jpg'),(2,'Bảo hiểm vật chất moto - xe máy','Các loại xe mô tô, xe máy có đăng ký hợp lệ.','Áp dụng cho trường hợp phương tiện bị cháy nổ.','Bảo hiểm cháy nổ:`   - Bồi thường cho Chủ xe những thiệt hại vật chất xe xảy ra do hỏa hoạn, cháy, nổ.`   - Trong mọi trường hợp, tổng số tiền bồi thường của chúng tôi không vượt quá số tiền bảo hiểm và/hoặc giá trị bảo hiểm đã ghi trên Giấy chứng nhận bảo hiểm.`Bồi thường cho Chủ xe những thiệt hại vật chất xe xảy ra do tai nạn bất ngờ, ngoài sự kiểm soát của Chủ xe, Lái xe trong những trường hợp sau đây:`   - Đâm, va, lật, đổ.`   - Những tai nạn bất khả kháng do thiên nhiên: Bão, lũ lụt, sét đánh, động đất, mưa đá, sụt lở.`   - Vật thể khác tác động lên xe.`   - Tai nạn do rủi ro bất ngờ khác gây nên (không bao gồm nguyên nhân hỏa hoạn, cháy, nổ; mất cắp, mất cướp toàn bộ xe).','02.jpg'),(3,'Bảo hiểm trộm, cướp toàn bộ xe moto - xe máy','- Xe mô tô, xe máy được sản xuất bởi chính hãng sản xuất: HONDA, YAMAHA, SYM, SUZUKI, PIAGGIO.`- Xe có thời gian sử dụng tối đa 18 năm kể từ năm đăng ký lần đầu, kể cả trường hợp xe tham gia bảo hiểm liên tục.','Áp dụng cho trường hợp bị mất cắp toàn bộ chiếc xe.','Tổn thất toàn bộ do trộm, cướp trong các trường hợp sau:`-Mất cắp tại các điểm trông giữ xe của trường học, bệnh viện, cơ quan, tòa nhà, bãi giữ xe công cộng, các tổ chức khác có giấy phép do cơ quan nhà nước có thẩm quyền cấp phép hoạt động đồng thời có phát phiếu giữ xe.`- Nhà, nơi cư trú bị trộm cướp đột nhập, cạy phá có dấu vết để lại và/hoặc bị tấn công hoặc đe dọa tấn công bằng vũ lực hoặc bị cướp.`- Mất do các nguyên nhân không nêu ở mục (a) và (b) trên.','03.jpg');
/*!40000 ALTER TABLE `insurances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paid_status`
--

DROP TABLE IF EXISTS `paid_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `paid_status` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paid_status`
--

LOCK TABLES `paid_status` WRITE;
/*!40000 ALTER TABLE `paid_status` DISABLE KEYS */;
INSERT INTO `paid_status` VALUES (1,'Chưa thanh toán'),(2,'Đã thanh toán');
/*!40000 ALTER TABLE `paid_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_histories`
--

DROP TABLE IF EXISTS `payment_histories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payment_histories` (
  `paymenthistory_id` int NOT NULL AUTO_INCREMENT,
  `contract_id` int DEFAULT NULL,
  `method` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8_general_ci NOT NULL,
  `amount` int NOT NULL,
  `paid_time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`paymenthistory_id`),
  KEY `contractID` (`contract_id`),
  CONSTRAINT `FK7nhuhpyd3f4d2dtgwk1yjgh2u` FOREIGN KEY (`contract_id`) REFERENCES `contracts` (`contract_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_histories`
--

LOCK TABLES `payment_histories` WRITE;
/*!40000 ALTER TABLE `payment_histories` DISABLE KEYS */;
INSERT INTO `payment_histories` VALUES (1,1,'Momo',4000000,'2022-07-12 08:40:40'),(5,4,'Momo',4000000,'2022-07-12 08:38:03'),(6,4,'Momo',4000000,'2022-07-12 08:39:51'),(7,4,'Momo',4000000,'2022-07-12 08:41:38');
/*!40000 ALTER TABLE `payment_histories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prices`
--

DROP TABLE IF EXISTS `prices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `prices` (
  `price_id` int NOT NULL AUTO_INCREMENT,
  `price` int NOT NULL,
  PRIMARY KEY (`price_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prices`
--

LOCK TABLES `prices` WRITE;
/*!40000 ALTER TABLE `prices` DISABLE KEYS */;
INSERT INTO `prices` VALUES (1,20000000),(2,40000000),(3,80000000);
/*!40000 ALTER TABLE `prices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `punishments`
--

DROP TABLE IF EXISTS `punishments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `punishments` (
  `punishment_id` int NOT NULL AUTO_INCREMENT,
  `contract_id` int DEFAULT NULL,
  `amount` int NOT NULL,
  `status_id` int NOT NULL,
  `resolve_time` datetime DEFAULT NULL,
  `paid_status_id` int DEFAULT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`punishment_id`),
  KEY `contractID` (`contract_id`),
  KEY `status` (`status_id`),
  KEY `FK4qw2wraetkdfoab1uymyyx6iv` (`paid_status_id`),
  CONSTRAINT `FK4qw2wraetkdfoab1uymyyx6iv` FOREIGN KEY (`paid_status_id`) REFERENCES `paid_status` (`id`),
  CONSTRAINT `punishments_ibfk_1` FOREIGN KEY (`contract_id`) REFERENCES `contracts` (`contract_id`),
  CONSTRAINT `punishments_ibfk_2` FOREIGN KEY (`status_id`) REFERENCES `status` (`status_id`),
  CONSTRAINT `punishments_ibfk_3` FOREIGN KEY (`status_id`) REFERENCES `status` (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `punishments`
--

LOCK TABLES `punishments` WRITE;
/*!40000 ALTER TABLE `punishments` DISABLE KEYS */;
INSERT INTO `punishments` VALUES (4,1,500000,3,NULL,1,'2022-07-13 22:11:16'),(5,1,500000,3,NULL,1,'2022-07-13 22:11:28');
/*!40000 ALTER TABLE `punishments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `request_compensation`
--

DROP TABLE IF EXISTS `request_compensation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `request_compensation` (
  `contract_id` int NOT NULL,
  `status_id` int DEFAULT NULL,
  `amount` int NOT NULL,
  `request_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `resolve_time` datetime DEFAULT NULL,
  PRIMARY KEY (`contract_id`),
  KEY `request_compensation_status__fk` (`status_id`),
  CONSTRAINT `request_compensation_contracts__fk` FOREIGN KEY (`contract_id`) REFERENCES `contracts` (`contract_id`),
  CONSTRAINT `request_compensation_status__fk` FOREIGN KEY (`status_id`) REFERENCES `status` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `request_compensation`
--

LOCK TABLES `request_compensation` WRITE;
/*!40000 ALTER TABLE `request_compensation` DISABLE KEYS */;
/*!40000 ALTER TABLE `request_compensation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `role_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'USER'),(2,'STAFF'),(3,'ADMIN');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `status` (
  `status_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status`
--

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
INSERT INTO `status` VALUES (1,'Đã xác nhận'),(2,'Từ chối'),(3,'Đang chờ');
/*!40000 ALTER TABLE `status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_accident`
--

DROP TABLE IF EXISTS `status_accident`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `status_accident` (
  `status_status_id` int NOT NULL,
  `accident_contract_id` int NOT NULL,
  PRIMARY KEY (`status_status_id`,`accident_contract_id`),
  UNIQUE KEY `UK_25j6dk2gk703t45jy9gd8wvbm` (`accident_contract_id`),
  CONSTRAINT `FK19884r7knme0q7y94dxwdtm3` FOREIGN KEY (`status_status_id`) REFERENCES `status` (`status_id`),
  CONSTRAINT `FK992u11r6yxosm11d5tp4w3wq4` FOREIGN KEY (`accident_contract_id`) REFERENCES `accidents` (`contract_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_accident`
--

LOCK TABLES `status_accident` WRITE;
/*!40000 ALTER TABLE `status_accident` DISABLE KEYS */;
/*!40000 ALTER TABLE `status_accident` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_punishment`
--

DROP TABLE IF EXISTS `status_punishment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `status_punishment` (
  `status_status_id` int NOT NULL,
  `punishment_punishment_id` int NOT NULL,
  PRIMARY KEY (`status_status_id`,`punishment_punishment_id`),
  UNIQUE KEY `UK_kie0ivlrdhr556muhr9p4soo1` (`punishment_punishment_id`),
  CONSTRAINT `FKaiofiuf6cl2951j1e5jq8cjax` FOREIGN KEY (`punishment_punishment_id`) REFERENCES `punishments` (`punishment_id`),
  CONSTRAINT `FKhscejhasr2ddle81ailbkblxw` FOREIGN KEY (`status_status_id`) REFERENCES `status` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_punishment`
--

LOCK TABLES `status_punishment` WRITE;
/*!40000 ALTER TABLE `status_punishment` DISABLE KEYS */;
/*!40000 ALTER TABLE `status_punishment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_request_compensation`
--

DROP TABLE IF EXISTS `status_request_compensation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `status_request_compensation` (
  `status_status_id` int NOT NULL,
  `request_compensation_contract_id` int NOT NULL,
  PRIMARY KEY (`status_status_id`,`request_compensation_contract_id`),
  UNIQUE KEY `UK_pihdxbwwbqt3et0q6el3xcu8w` (`request_compensation_contract_id`),
  CONSTRAINT `FK8v69emhljfua7q0578e8r056y` FOREIGN KEY (`request_compensation_contract_id`) REFERENCES `request_compensation` (`contract_id`),
  CONSTRAINT `FKj1rkhl8y3xa8rvwrc5co59oig` FOREIGN KEY (`status_status_id`) REFERENCES `status` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_request_compensation`
--

LOCK TABLES `status_request_compensation` WRITE;
/*!40000 ALTER TABLE `status_request_compensation` DISABLE KEYS */;
/*!40000 ALTER TABLE `status_request_compensation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `account_id` int NOT NULL,
  `gender` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8_general_ci DEFAULT NULL,
  `phone_number` varchar(11) DEFAULT NULL,
  `address` varchar(800) CHARACTER SET utf8mb3 COLLATE utf8_general_ci DEFAULT NULL,
  `first_name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8_general_ci DEFAULT NULL,
  `middle_name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8_general_ci DEFAULT NULL,
  `last_name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8_general_ci DEFAULT NULL,
  `DOB` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`account_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Nam','0966070792','FPT university, Thach That, Hoa Lac','Do','Duy','Thai','2000-10-15','thaiddhe140504@fpt.edu.vn'),(2,'Nam','0123456789','đh fpt, thạch thất, hà nội','Nguyễn','Văn','A','1984-01-01','user01@gmail.com'),(3,'Nữ','0123456789','đh fpt, thạch thất, hà nội','Nguyễn','Thị','C','1997-12-28','user02@gmail.com'),(4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,'Nữ','0123456789','đh fpt, thạch thất, hà nội','Nguyễn','Thị','User03','1984-01-01','user03@gmail.com'),(21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(22,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(23,'Nam','0123456789','hà nội','demo','demo','demo','2022-07-10',NULL),(24,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(25,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(26,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(27,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(28,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(29,'Nam','0966070792','Dai hoc FPT','Duy','Duy','Thai','2022-07-22','duythai1315@gmail.com'),(30,'Nam','0966070792','Dai hoc FPT','Duy','Thị','Thai','2022-07-30','duythai1315@gmail.com');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_roles`
--

DROP TABLE IF EXISTS `users_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_roles` (
  `account_id` int NOT NULL,
  `role_id` int NOT NULL,
  KEY `user_fk_idx` (`account_id`),
  KEY `role_fk_idx` (`role_id`),
  CONSTRAINT `role_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`),
  CONSTRAINT `user_fk` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_roles`
--

LOCK TABLES `users_roles` WRITE;
/*!40000 ALTER TABLE `users_roles` DISABLE KEYS */;
INSERT INTO `users_roles` VALUES (1,3),(3,1),(4,2),(11,2),(12,1),(2,1),(21,1),(22,1),(23,1),(24,2),(25,1),(26,2),(27,1),(28,1),(29,1),(30,1);
/*!40000 ALTER TABLE `users_roles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-07-19 16:29:36
