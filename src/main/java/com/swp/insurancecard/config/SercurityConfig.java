package com.swp.insurancecard.config;

import com.swp.insurancecard.service.AccountService;
import org.springframework.context.annotation.*;
import org.springframework.security.authentication.dao.*;
import org.springframework.security.config.annotation.authentication.builders.*;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.*;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.annotation.Resource;

@Configuration
@EnableWebSecurity
public class SercurityConfig extends WebSecurityConfigurerAdapter {
    @Bean
    public UserDetailsService userDetailsService(){
        return new AccountService();
    }


    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService());
        authProvider.setPasswordEncoder(passwordEncoder());

        return authProvider;
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider());
    }
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/about","/ProductDetail/**","/login_fail","/home","/forgot-password","/password/**","/register","/save","/vendor/**","/css/**","/img/**","/js/**", "/src/**").permitAll()
                .antMatchers("/fill_info/**","/dashboard").hasAnyAuthority("USER","STAFF","ADMIN")
                .antMatchers("/requestContract/**","/profile/**","/history/**","/contracts/user/**","/contracts/checkout/**","/paymentHistory/user/**","/accidents/user/**","/punishments/user/**","/compensations/**").hasAuthority("USER")
                .antMatchers("/resolvePunishment/**","/request/**","/contracts/admin/**","/manageUser/staff","/customerInfo/**","/resolveAccident/**", "/resolveContract/**","/resolveCompensation/**").hasAnyAuthority("STAFF","ADMIN")
                .antMatchers("/manageAccount/**").hasAuthority("ADMIN")
                .and()
                .rememberMe().userDetailsService(userDetailsService())
                .and()
                .formLogin().loginPage("/login").permitAll()
                .defaultSuccessUrl("/home")
                .failureUrl("/login_fail")
                .loginProcessingUrl("/j_spring_security_check")
                .and()
                .logout().logoutSuccessUrl("/home")

        ;
    }
}
