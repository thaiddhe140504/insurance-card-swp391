package com.swp.insurancecard.controllers;

import com.swp.insurancecard.models.Accident;
import com.swp.insurancecard.models.Account;
import com.swp.insurancecard.models.User;
import com.swp.insurancecard.repositories.AccidentRepository;
import com.swp.insurancecard.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/accidents")
public class AccidentController {
    @Autowired
    AccidentRepository accidentRepository;
    @Autowired
    AccountRepository accountRepository;
    @GetMapping("/user")
    String viewAccident(Model model){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        Account account = accountRepository.getAccountByUserName(currentPrincipalName);
        if (account.getInfoStatus() == 0){
            User user = new User();
            model.addAttribute("user",user);
            return "fillInfo";
        }
        List<Accident> accidents = accidentRepository.findAllByAccountId(account.getId());
        model.addAttribute("accidents", accidents);
        return "accidents";
    }
}
