package com.swp.insurancecard.controllers;

import com.swp.insurancecard.models.*;
import com.swp.insurancecard.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/compensations")
public class CompensationController {

    @Autowired
    AccountRepository accountRepository;
    @Autowired
    CompensationRepository compensationRepository;
    @Autowired
    ContractRepository contractRepository;

    @Autowired
    AccidentRepository accidentRepository;
    @Autowired
    StatusRepository statusRepository;

    @GetMapping("/user")
    String viewCompensation(Model model){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        Account account = accountRepository.getAccountByUserName(currentPrincipalName);
        if (account.getInfoStatus() == 0){
            User user = new User();
            model.addAttribute("user",user);
            return "fillInfo";
        }
        List<RequestCompensation> requestCompensations = compensationRepository.findAllByAccountId(account.getId());
        model.addAttribute("requestCompensations", requestCompensations);
        return "historyCompensation";
    }

    @GetMapping("/request")
    String requestCompensation(Model model){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        Account account = accountRepository.getAccountByUserName(currentPrincipalName);
        if (account.getInfoStatus() == 0){
            User user = new User();
            model.addAttribute("user",user);
            return "fillInfo";
        }
        List<Contract> contracts = contractRepository.findbyAccountIdAndStatus(account.getId(),2);
        List<Contract> contracts1 = new ArrayList<>();
        for (Contract contract : contracts){
            if(contract.getAccident()==null || contract.getAccident().getStatus().getId() == 2){
                contracts1.add(contract);
            }
        }
        Accident accident = new Accident();
        model.addAttribute("contracts",contracts1);
        model.addAttribute("accident",accident);
        return "requestCompensation";
    }

    @PostMapping("/request")
    String saveRequestCompensation(@ModelAttribute("accident") Accident accident) throws ParseException {

        Date test = new SimpleDateFormat("dd MMMM yyyy hh:mm a").parse(accident.getOccurTime());
        Timestamp timestamp = new java.sql.Timestamp(test.getTime());
        accident.setOccurTime(timestamp.toString());
        accident.setStatus(statusRepository.getReferenceById(3));
        accidentRepository.save(accident);
        return "redirect:/accidents/user";
    }
}
