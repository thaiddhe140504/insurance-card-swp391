package com.swp.insurancecard.controllers;

import com.swp.insurancecard.models.*;
import com.swp.insurancecard.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("/contracts")
public class ContractController {
    @Autowired
    ContractRepository contractRepository;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    InsuranceRepository insuranceRepository;

    @Autowired
    DurationRepository durationRepository;

    @Autowired
    PriceRepository priceRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ContractStatusRepository contractStatusRepository;

    @Autowired
    PaymentHistoryRepository paymentHistoryRepository;

    @GetMapping("/user")
    public String viewUserContracts(Model model){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        Account account = accountRepository.getAccountByUserName(currentPrincipalName);
        if (account.getInfoStatus() == 0){
            User user = new User();
            model.addAttribute("user",user);
            return "fillInfo";
        }
        List<Contract> contracts = contractRepository.findbyAccountId(account.getId());
        model.addAttribute("contracts", contracts);
        return "contracts";
    }

    @GetMapping("/checkout/{id}")
    String viewCheckoutContract(@PathVariable("id")Integer id, Model model){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        Account account = accountRepository.getAccountByUserName(currentPrincipalName);
        if (account.getInfoStatus() == 0){
            User user = new User();
            model.addAttribute("user",user);
            return "fillInfo";
        }
        PaymentHistory paymentHistory = new PaymentHistory();
        model.addAttribute("paymentHistory",paymentHistory);
        Contract contract = contractRepository.getReferenceById(id);
        model.addAttribute("contract", contract);
        return "checkoutContract";
    }
    @PostMapping("/checkout/{id}")
    String checkoutContract(@ModelAttribute("paymentHistory") PaymentHistory paymentHistory, @PathVariable("id")Integer id){
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        paymentHistory.setPaidTime(timestamp.toString());
        paymentHistory.setAmount(4000000);
        paymentHistory.setContractId(id);
        paymentHistoryRepository.save(paymentHistory);
        return "redirect:/paymentHistory/user";
    }

    @GetMapping("/admin")
    public String viewContracts(Model model){
        List<Contract> contracts = contractRepository.findbyWithoutStatuses(1,8);
        model.addAttribute("contracts", contracts);
        return "contracts";
    }

    @GetMapping("/staff/create")
    String createContract(Model model){
        List<Duration> durations = durationRepository.findAll();
        List<Price> prices = priceRepository.findAll();
        List<Insurance> insurances = insuranceRepository.findAll();
        List<User> users = userRepository.findAll();
        List<User> users1 = new ArrayList<>();

        for(User user : users){
            for(Role role : user.getAccount().getRoles()){
                if(role.getName().equals("USER")){
                    users1.add(user);
                }
            }

        }
        Contract contract = new Contract();
        model.addAttribute("durations",durations);
        model.addAttribute("prices",prices);
        model.addAttribute("insurances",insurances);
        model.addAttribute("users",users1);
        model.addAttribute("contract",contract);
        return "createContract";
    }

    @PostMapping("/create")
    String createContract(@ModelAttribute("contract") Contract contract){
        contract.setContractStatus(contractStatusRepository.getReferenceById(2));

        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        contract.setCreateTime(timestamp.toString());
        contractRepository.save(contract);
        return "redirect:/contracts/admin";
    }

    @GetMapping("/renew/{id}")
    String renewContract(@PathVariable("id")Integer id){
        Contract contract = contractRepository.getReferenceById(id);
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        contract.setRenewTime(timestamp.toString());
        contract.setContractStatus(contractStatusRepository.getReferenceById(2));
        contractRepository.save(contract);
        return "redirect:/contracts/admin";
    }

}
