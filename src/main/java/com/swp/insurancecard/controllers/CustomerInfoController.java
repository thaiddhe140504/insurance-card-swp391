package com.swp.insurancecard.controllers;

import com.swp.insurancecard.models.Account;
import com.swp.insurancecard.models.User;
import com.swp.insurancecard.repositories.AccountRepository;
import com.swp.insurancecard.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/customerInfo")
public class CustomerInfoController {

    @Autowired
    AccountRepository accountRepository;
    @Autowired
    UserRepository userRepository;

    @GetMapping("/{id}")
    String viewCustomerInfo(@PathVariable Long id, Model model){
        Account account = accountRepository.getReferenceById(id);
        User user = userRepository.getReferenceById(id);
        model.addAttribute("account", account);
        model.addAttribute("user", user);
        return "detailUser";
    }

    @GetMapping("/update/{id}")
    String editCustomerInfo(@PathVariable Long id, Model model){
        Account account = accountRepository.getReferenceById(id);
        User user = userRepository.getReferenceById(id);
        model.addAttribute("account", account);
        model.addAttribute("user", user);
        return "editUser";
    }
    @PostMapping("/update/save/{id}")
    String saveCustomerInfo(@PathVariable Long id, @ModelAttribute("user")User user){
        User user1 = userRepository.getReferenceById(id);
        user1.setDOB(user.getDOB());
        user1.setPhoneNumber(user.getPhoneNumber());
        user1.setLastName(user.getLastName());
        user1.setMiddleName(user.getMiddleName());
        user1.setAddress(user.getAddress());
        user1.setFirstName(user.getFirstName());
        user1.setEmail(user.getEmail());
        user1.setGender(user.getGender());
        userRepository.save(user1);
        return "redirect:/customerInfo/"+id;
    }
}
