package com.swp.insurancecard.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController {
    @RequestMapping(path = "/login")
    public String viewLogin(Model model){
        return "login";
    }

    @RequestMapping(path = "/login_fail")
    public String viewLoginAgain(Model model){
        model.addAttribute("loginFail",true);
        return "login";
    }

}
