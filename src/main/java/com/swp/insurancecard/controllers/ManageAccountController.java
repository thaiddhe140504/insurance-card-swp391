package com.swp.insurancecard.controllers;

import com.swp.insurancecard.models.Account;
import com.swp.insurancecard.models.Role;
import com.swp.insurancecard.models.User;
import com.swp.insurancecard.repositories.AccountRepository;
import com.swp.insurancecard.repositories.RoleRepository;
import com.swp.insurancecard.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/manageAccount")
public class ManageAccountController {
    @Autowired
    AccountRepository accountRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    UserRepository userRepository;

    @GetMapping()
    String viewAccount(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        Account account = accountRepository.getAccountByUserName(currentPrincipalName);
        List<Account> accounts = accountRepository.getAccountsByIdExcludeId(account.getId());
        model.addAttribute("accounts", accounts);
        return "manageAccount";
    }

    @GetMapping("/role/{id}")
    String viewEditRole(@PathVariable("id") Long id, Model model) {
        List<Role> roleList = roleRepository.findAll();
        Account account = accountRepository.getReferenceById(id);
        model.addAttribute("roleList", roleList);
        model.addAttribute("account", account);
        return "editRole";
    }

    @PostMapping("/role/change/{id}")
    String changeRole(@PathVariable("id") Long id, @ModelAttribute("account") Account account) {
        Account account1 = accountRepository.getReferenceById(id);
        account1.setRoles(account.getRoles());
        accountRepository.save(account1);
        return "redirect:/manageAccount";
    }

    @GetMapping("/create")
    String viewCreateAccount(Model model) {
        Account account = new Account();
        List<Role> roleList = roleRepository.findAll();
        model.addAttribute("roleList", roleList);
        model.addAttribute("account", account);
        return "createAccount";
    }

    @PostMapping("/create/save")
    String saveAccount(@ModelAttribute("account") Account account, Model model){
        Account storeAcc = accountRepository.getAccountByUserName(account.getUsername());
        if(storeAcc != null){
            List<Role> roleList = roleRepository.findAll();
            model.addAttribute("roleList", roleList);
            model.addAttribute("msgError","Tên đăng nhập đã tồn tại");
            return "createAccount";
        }
        account.encodePassword();
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        account.setCreateTime(timestamp.toString());
        account.setInfoStatus(0);

        Account a =  accountRepository.save(account);
        userRepository.save(new User(a.getId()));
        return "redirect:/manageAccount";
    }
}
