package com.swp.insurancecard.controllers;

import com.swp.insurancecard.models.Account;
import com.swp.insurancecard.models.Role;
import com.swp.insurancecard.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("/manageUser")
public class ManageCustomerController {
    @Autowired
    AccountRepository accountRepository;

    @GetMapping("/staff")
    String viewManageUser(Model model){
        List<Account> accounts = accountRepository.findAll();
        List<Account> accountList = new ArrayList<>();
        for(Account account : accounts){
            for(Role role : account.getRoles()){
                if(role.getName().equals("USER")){
                    accountList.add(account);
                }
            }

        }
        model.addAttribute("accountList", accountList);
        return "manageUser";
    }
}
