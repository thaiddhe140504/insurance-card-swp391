package com.swp.insurancecard.controllers;

import com.swp.insurancecard.models.Account;
import com.swp.insurancecard.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class PasswordController {
    @Autowired
    AccountRepository accountRepository;

    @RequestMapping("/forgot-password")
    String viewForgotPassword(Model model){
        Account account = new Account();
        model.addAttribute("account",account);
        return "resetPassword";
    }

    @RequestMapping(value = "/password/save", method = RequestMethod.POST)
    public String updatePassword(@ModelAttribute("account") Account account, RedirectAttributes redirectAttributes){
        Account account1 = accountRepository.getAccountByUserName(account.getUsername());
        account1.setPassword(account.getPassword());
        account1.encodePassword();
        accountRepository.save(account1);
        redirectAttributes.addFlashAttribute("pass",true);
        return "redirect:/login";
    }
}
