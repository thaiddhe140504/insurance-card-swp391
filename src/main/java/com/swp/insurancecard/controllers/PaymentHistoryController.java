package com.swp.insurancecard.controllers;

import com.swp.insurancecard.models.Account;
import com.swp.insurancecard.models.PaymentHistory;
import com.swp.insurancecard.models.User;
import com.swp.insurancecard.repositories.AccountRepository;
import com.swp.insurancecard.repositories.PaymentHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/paymentHistory")
public class PaymentHistoryController {
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    PaymentHistoryRepository paymentHistoryRepository;

    @GetMapping("/user")
    String viewPaymentHistory(Model model){

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        Account account = accountRepository.getAccountByUserName(currentPrincipalName);
        if (account.getInfoStatus() == 0){
            User user = new User();
            model.addAttribute("user",user);
            return "fillInfo";
        }
        List<PaymentHistory> paymentHistories = paymentHistoryRepository.findAllByAccountId(account.getId());
        model.addAttribute("paymentHistories", paymentHistories);
        return "paymentHistory";
    }
}
