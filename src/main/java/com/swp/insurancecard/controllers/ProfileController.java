package com.swp.insurancecard.controllers;

import com.swp.insurancecard.models.Account;
import com.swp.insurancecard.models.User;
import com.swp.insurancecard.repositories.AccountRepository;
import com.swp.insurancecard.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/profile")
public class ProfileController {
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    UserRepository userRepository;

    boolean pass;
    boolean profile;

    @GetMapping("")
    public String viewProfile(Model model){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        Account account = accountRepository.getAccountByUserName(currentPrincipalName);
        if (account.getInfoStatus() == 0){
            User user = new User();
            model.addAttribute("user",user);
            return "fillInfo";
        }
        if (pass) model.addAttribute("pass",true);
        pass = false;
        if (profile) model.addAttribute("profile",true);
        profile = false;
        model.addAttribute("account", account);
        return "profile";
    }

    @GetMapping("/update")
    public String editProfile(Model model){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        Account account = accountRepository.getAccountByUserName(currentPrincipalName);
        if (account.getInfoStatus() == 0){
            User user = new User();
            model.addAttribute("user",user);
            return "fillInfo";
        }
        User user = userRepository.getReferenceById(account.getId());
        model.addAttribute("user", user);
        return "updateProfile";
    }

    @PostMapping("/update/save")
    public String updateProfile(@ModelAttribute("user") User user, Model model){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        Account account = accountRepository.getAccountByUserName(currentPrincipalName);
        if (account.getInfoStatus() == 0){
            User user1 = new User();
            model.addAttribute("user",user);
            return "fillInfo";
        }
        User user1 = userRepository.getReferenceById(account.getId());
        user1.setDOB(user.getDOB());
        user1.setPhoneNumber(user.getPhoneNumber());
        user1.setLastName(user.getLastName());
        user1.setMiddleName(user.getMiddleName());
        user1.setAddress(user.getAddress());
        user1.setFirstName(user.getFirstName());
        user1.setEmail(user.getEmail());
        user1.setGender(user.getGender());
        userRepository.save(user1);
        return "redirect:/profile";
    }

    @PostMapping("/password/save")
    public String changePassword(@ModelAttribute("account") Account account, Model model){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        Account account1 = accountRepository.getAccountByUserName(currentPrincipalName);
        if (account1.getInfoStatus() == 0){
            User user = new User();
            model.addAttribute("user",user);
            return "fillInfo";
        }
        account.encodePassword();
        account1.setPassword(account.getPassword());
        accountRepository.save(account1);
        pass =true;
        return "redirect:/profile";
    }
}
