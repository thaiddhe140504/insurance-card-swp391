package com.swp.insurancecard.controllers;

import com.swp.insurancecard.models.Account;
import com.swp.insurancecard.models.Punishment;
import com.swp.insurancecard.models.User;
import com.swp.insurancecard.repositories.AccountRepository;
import com.swp.insurancecard.repositories.PaidStatusRepository;
import com.swp.insurancecard.repositories.PunishmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/punishments")
public class PunishmentController {

    @Autowired
    PunishmentRepository punishmentRepository;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    PaidStatusRepository paidStatusRepository;

    @GetMapping("/user")
    String viewPunishment(Model model){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        Account account = accountRepository.getAccountByUserName(currentPrincipalName);
        if (account.getInfoStatus() == 0){
            User user = new User();
            model.addAttribute("user",user);
            return "fillInfo";
        }
        List<Punishment> punishments = punishmentRepository.findAllByAccountId(account.getId());
        model.addAttribute("punishments", punishments);
        return "punishments";
    }

    @GetMapping("/checkout/{id}")
    String checkoutPunishment(@PathVariable("id") Integer id, Model model){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        Account account = accountRepository.getAccountByUserName(currentPrincipalName);
        if (account.getInfoStatus() == 0){
            User user = new User();
            model.addAttribute("user",user);
            return "fillInfo";
        }
        Punishment punishment = punishmentRepository.getReferenceById(id);
        punishment.setPaidStatus(paidStatusRepository.getReferenceById(2));
        punishmentRepository.save(punishment);
        return "redirect:/punishments/user";
    }
}
