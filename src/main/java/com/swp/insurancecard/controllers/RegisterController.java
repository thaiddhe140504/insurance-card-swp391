package com.swp.insurancecard.controllers;

import com.swp.insurancecard.models.Account;
import com.swp.insurancecard.models.User;
import com.swp.insurancecard.repositories.AccountRepository;
import com.swp.insurancecard.repositories.RoleRepository;
import com.swp.insurancecard.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
//@RequestMapping("/register")
public class RegisterController {
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    RoleRepository roleRepository;

    @Autowired
    UserRepository userRepository;
    @GetMapping("/register")
    public String viewRegister(Model model){

        Account account = new Account();
        model.addAttribute("account",account);
        return "register";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveAccount(@ModelAttribute("account") Account account, Model model){
        Account storeAcc = accountRepository.getAccountByUserName(account.getUsername());
        if(storeAcc != null){
            model.addAttribute("msgError","Tên đăng nhập đã tồn tại");
            return "register";
        }
        account.encodePassword();
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        account.setCreateTime(timestamp.toString());
        account.addRole(roleRepository.getReferenceById(1));
        Account a =  accountRepository.save(account);
        userRepository.save(new User(a.getId()));
        model.addAttribute("success",true);
        return "login";
    }
}
