package com.swp.insurancecard.controllers;

import com.swp.insurancecard.models.*;
import com.swp.insurancecard.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/requestContract")
public class RequestContractController {
    @Autowired
    InsuranceRepository insuranceRepository;
    @Autowired
    DurationRepository durationRepository;
    @Autowired
    PriceRepository priceRepository;
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    ContractRepository contractRepository;

    @Autowired
    ContractStatusRepository contractStatusRepository;


    @GetMapping("")
    String viewRequestContract(Model model){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        Account account = accountRepository.getAccountByUserName(currentPrincipalName);
        if (account.getInfoStatus() == 0){
            User user = new User();
            model.addAttribute("user",user);
            return "fillInfo";
        }
        List<Insurance> insuranceList = insuranceRepository.findAll();
        List<Price> priceList = priceRepository.findAll();
        List<Duration> durationList = durationRepository.findAll();
        Contract contract = new Contract();
        model.addAttribute("insuranceList",insuranceList);
        model.addAttribute("priceList",priceList);
        model.addAttribute("durationList",durationList);
        model.addAttribute("contract",contract);
        return "requestContract";
    }

    @PostMapping("/request")
    String requestContract(@ModelAttribute("contract") Contract contract, Model model){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        Account account = accountRepository.getAccountByUserName(currentPrincipalName);
        if (account.getInfoStatus() == 0){
            User user = new User();
            model.addAttribute("user",user);
            return "fillInfo";
        }
        contract.setUser(userRepository.getReferenceById(account.getId()));
        contract.setContractStatus(contractStatusRepository.getReferenceById(1));
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        contract.setCreateTime(timestamp.toString());
        contractRepository.save(contract);
        return "redirect:/contracts/user";
    }

    @GetMapping("/renew/{id}")
    String renewContract(@PathVariable("id")Integer id, Model model){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        Account account = accountRepository.getAccountByUserName(currentPrincipalName);
        if (account.getInfoStatus() == 0){
            User user = new User();
            model.addAttribute("user",user);
            return "fillInfo";
        }
        Contract contract = contractRepository.getReferenceById(id);
        contract.setContractStatus(contractStatusRepository.getReferenceById(3));
        contractRepository.save(contract);
        return "redirect:/contracts/user";
    }

    @GetMapping("/cancel/{id}")
    String cancelContract(@PathVariable("id")Integer id, Model model){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        Account account = accountRepository.getAccountByUserName(currentPrincipalName);
        if (account.getInfoStatus() == 0){
            User user = new User();
            model.addAttribute("user",user);
            return "fillInfo";
        }
        Contract contract = contractRepository.getReferenceById(id);
        contract.setContractStatus(contractStatusRepository.getReferenceById(5));
        contractRepository.save(contract);
        return "redirect:/contracts/user";
    }
}
