package com.swp.insurancecard.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/request")
public class RequestController {
    @GetMapping("")
    public String viewRequestMenu(){
        return "request";
    }
}
