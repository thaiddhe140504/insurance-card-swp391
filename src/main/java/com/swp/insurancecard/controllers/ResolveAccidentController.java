package com.swp.insurancecard.controllers;

import com.swp.insurancecard.models.Accident;
import com.swp.insurancecard.models.Contract;
import com.swp.insurancecard.models.RequestCompensation;
import com.swp.insurancecard.repositories.AccidentRepository;
import com.swp.insurancecard.repositories.CompensationRepository;
import com.swp.insurancecard.repositories.ContractRepository;
import com.swp.insurancecard.repositories.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/resolveAccident")
public class ResolveAccidentController {

    @Autowired
    AccidentRepository accidentRepository;

    @Autowired
    StatusRepository statusRepository;

    @Autowired
    CompensationRepository compensationRepository;

    @Autowired
    ContractRepository contractRepository;

    @GetMapping("")
    String viewAccident(Model model){
        List<Accident> accidents = accidentRepository.findAll();
        model.addAttribute("accidents", accidents);
        return "resolveAccident";
    }

    @GetMapping("/agree/{id}")
    String agreeAccident(@PathVariable("id")Integer id){
       Accident accident = accidentRepository.getReferenceById(id);
       accident.setStatus(statusRepository.getReferenceById(1));
       accidentRepository.save(accident);
       Contract contract = contractRepository.getReferenceById(id);
       RequestCompensation requestCompensation = new RequestCompensation();
       requestCompensation.setAmount(contract.getPrice().getPrice()/100*80);
       requestCompensation.setStatus(statusRepository.getReferenceById(3));
       Date date = new Date();
       Timestamp timestamp = new Timestamp(date.getTime());
       requestCompensation.setRequestTime(timestamp.toString());
       compensationRepository.save(requestCompensation);
       return "redirect:/resolveAccident";
    }

    @GetMapping("/cancel/{id}")
    String cancelAccident(@PathVariable("id")Integer id){
        Accident accident = accidentRepository.getReferenceById(id);
        accident.setStatus(statusRepository.getReferenceById(2));
        accidentRepository.save(accident);
        return "redirect:/resolveAccident";
    }
}
