package com.swp.insurancecard.controllers;

import com.swp.insurancecard.models.Contract;
import com.swp.insurancecard.models.RequestCompensation;
import com.swp.insurancecard.repositories.CompensationRepository;
import com.swp.insurancecard.repositories.ContractRepository;
import com.swp.insurancecard.repositories.ContractStatusRepository;
import com.swp.insurancecard.repositories.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/resolveCompensation")
public class ResolveCompensationController {
    @Autowired
    CompensationRepository compensationRepository;

    @Autowired
    StatusRepository statusRepository;

    @Autowired
    ContractRepository contractRepository;

    @Autowired
    ContractStatusRepository contractStatusRepository;

    @GetMapping
    String viewRequestCompensation(Model model) {
        List<RequestCompensation> requestCompensations = compensationRepository.findAll();
        model.addAttribute("requestCompensations", requestCompensations);
        return "resolveCompensation";
    }

    @GetMapping("/agree/{id}")
    String agreeRequestCompensation(@PathVariable("id")Integer id) {
        RequestCompensation requestCompensation = compensationRepository.getReferenceById(id);
        requestCompensation.setStatus(statusRepository.getReferenceById(1));
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        requestCompensation.setResolveTime(timestamp.toString());
        compensationRepository.save(requestCompensation);
        Contract contract = contractRepository.getReferenceById(id);
        contract.setContractStatus(contractStatusRepository.getReferenceById(10));
        return "redirect:/resolveCompensation";
    }

    @GetMapping("/disagree/{id}")
    String disagreeRequestCompensation(@PathVariable("id")Integer id) {
        RequestCompensation requestCompensation = compensationRepository.getReferenceById(id);
        requestCompensation.setStatus(statusRepository.getReferenceById(2));
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        requestCompensation.setResolveTime(timestamp.toString());
        compensationRepository.save(requestCompensation);
        return "redirect:/resolveCompensation";
    }
}
