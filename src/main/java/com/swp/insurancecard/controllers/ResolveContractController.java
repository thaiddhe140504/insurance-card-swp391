package com.swp.insurancecard.controllers;

import com.swp.insurancecard.models.Contract;
import com.swp.insurancecard.repositories.ContractRepository;
import com.swp.insurancecard.repositories.ContractStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/resolveContract")
public class ResolveContractController {
    @Autowired
    ContractRepository contractRepository;
    @Autowired
    ContractStatusRepository contractStatusRepository;

    @GetMapping("/create")
    String resolveCreateContract(Model model){
        List<Contract> contracts = contractRepository.findbyStatuses(1,8);
        model.addAttribute("contracts", contracts);
        return "resolveContract";
    }

    @GetMapping("/create/agree/{id}")
    String agreeCreateContract(@PathVariable("id") Integer id){
        Contract contract = contractRepository.getReferenceById(id);
        contract.setContractStatus(contractStatusRepository.getReferenceById(2));
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        contract.setCreateTime(timestamp.toString());
        contractRepository.save(contract);
        return "redirect:/resolveContract/create";
    }

    @GetMapping("/cancel/{id}")
    String cancelCreateContract(@PathVariable("id") Integer id){
        Contract contract = contractRepository.getReferenceById(id);
        contract.setContractStatus(contractStatusRepository.getReferenceById(8));
        contractRepository.save(contract);
        return "redirect:/resolveContract/create";
    }

    @GetMapping("/renew")
    String resolveRenewContract(Model model){
        List<Contract> contracts = contractRepository.findbyStatus(3);
        model.addAttribute("contracts", contracts);
        return "resolveRenewContract";
    }

    @GetMapping("/renew/agree/{id}")
    String agreeRenewContract(@PathVariable("id") Integer id){
        Contract contract = contractRepository.getReferenceById(id);
        contract.setContractStatus(contractStatusRepository.getReferenceById(2));
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        contract.setRenewTime(timestamp.toString());
        contractRepository.save(contract);
        return "redirect:/resolveContract/renew";
    }

    @GetMapping("/renew/cancel/{id}")
    String disagreeRenewContract(@PathVariable("id") Integer id){
        Contract contract = contractRepository.getReferenceById(id);
        contract.setContractStatus(contractStatusRepository.getReferenceById(10));
        contractRepository.save(contract);
        return "redirect:/resolveContract/renew";
    }

    @GetMapping("/cancel")
    String resolveCancelContract(Model model){
        List<Contract> contracts = contractRepository.findbyStatus(5);
        model.addAttribute("contracts", contracts);
        return "resolveCancelContract";
    }

    @GetMapping("/cancel/agree/{id}")
    String agreeCancelContract(@PathVariable("id") Integer id){
        Contract contract = contractRepository.getReferenceById(id);
        contract.setContractStatus(contractStatusRepository.getReferenceById(10));
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        contract.setCancelTime(timestamp.toString());
        contractRepository.save(contract);
        return "redirect:/resolveContract/cancel";
    }

    @GetMapping("/cancel/cancel/{id}")
    String disagreeCancelContract(@PathVariable("id") Integer id){
        Contract contract = contractRepository.getReferenceById(id);
        contract.setContractStatus(contractStatusRepository.getReferenceById(7));
        contractRepository.save(contract);
        return "redirect:/resolveContract/renew";
    }
}
