package com.swp.insurancecard.controllers;

import com.swp.insurancecard.models.Contract;
import com.swp.insurancecard.models.Punishment;
import com.swp.insurancecard.repositories.ContractRepository;
import com.swp.insurancecard.repositories.PaidStatusRepository;
import com.swp.insurancecard.repositories.PunishmentRepository;
import com.swp.insurancecard.repositories.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/resolvePunishment")
public class ResolvePunishmentController {
    @Autowired
    PunishmentRepository punishmentRepository;

    @Autowired
    StatusRepository statusRepository;

    @Autowired
    ContractRepository contractRepository;

    @Autowired
    PaidStatusRepository paidStatusRepository;

    @GetMapping
    String viewPunishment(Model model){
        List<Punishment> punishmentList = punishmentRepository.findAll();
        model.addAttribute("punishmentList", punishmentList);
        return "resolvePunishment";
    }

    @GetMapping("/{id}")
    String resolvePunishment(@PathVariable("id") Integer id){
        Punishment punishment = punishmentRepository.getReferenceById(id);
        punishment.setStatus(statusRepository.getReferenceById(1));
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        punishment.setResolveTime(timestamp.toString());
        punishmentRepository.save(punishment);
        return "redirect:/resolvePunishment";
    }

    @GetMapping("/create/{id}")
    String createPunishment(@PathVariable("id") Integer id, Model model){
        Contract contract = contractRepository.getReferenceById(id);
        model.addAttribute("contract", contract);
        Punishment punishment = new Punishment();
        model.addAttribute("punishment", punishment);
        return "createPunishment";
    }

    @PostMapping("/create/{id}")
    String savePunsiment(@PathVariable("id") Integer id, @ModelAttribute("punishment") Punishment punishment){
        punishment.setAmount(500000);
        punishment.setContractId(id);
        punishment.setPaidStatus(paidStatusRepository.getReferenceById(1));
        punishment.setStatus(statusRepository.getReferenceById(3));
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        punishment.setCreateTime(timestamp.toString());
        punishmentRepository.save(punishment);
        return "redirect:/resolvePunishment";
    }
}
