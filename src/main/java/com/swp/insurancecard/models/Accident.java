package com.swp.insurancecard.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "accidents")
public class Accident {
    @Id
    @Column(name = "contract_id")
    Integer id;

    @OneToOne(mappedBy = "accident",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    Contract contract;

    String detail;
    String occurTime;

    @ManyToOne
    @JoinColumn(name="status_id")
    Status status;

    String resolveTime;
}
