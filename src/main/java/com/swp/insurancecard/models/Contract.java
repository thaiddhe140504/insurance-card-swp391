package com.swp.insurancecard.models;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "contracts")
public class Contract implements Serializable {
    @Id
    @Column(name = "contract_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    @ManyToOne
    @JoinColumn(name = "account_id")
    User user;

    @ManyToOne
    @JoinColumn(name = "price_id")
    Price price;

    @ManyToOne
    @JoinColumn(name = "duration_id")
    Duration duration;

    @ManyToOne
    @JoinColumn(name = "insurance_id")
    Insurance insurance;


    String createTime;

    String renewTime;

    String cancelTime;

    @OneToMany(mappedBy = "contract", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    Set<PaymentHistory> paymentHistories;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    Accident accident;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    RequestCompensation requestCompensation;

    @OneToMany(mappedBy = "contract", cascade = CascadeType.ALL)
    Set<Punishment> punishments;

    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name="contract_status_id", referencedColumnName = "contract_status_id")
    ContractStatus contractStatus;

}
