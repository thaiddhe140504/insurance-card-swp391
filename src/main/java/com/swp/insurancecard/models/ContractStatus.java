package com.swp.insurancecard.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "contract_status")
public class ContractStatus {
    @Id
    @Column(name = "contract_status_id")
    Integer id;

    String name;

    @OneToOne(mappedBy ="contractStatus", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    Contract contract;

}
