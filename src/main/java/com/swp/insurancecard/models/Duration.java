package com.swp.insurancecard.models;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity(name = "durations")
public class Duration {
    @Id
    @Column(name = "duration_id")
    Integer id;
    int duration;

    @OneToMany(mappedBy = "duration", cascade = CascadeType.ALL)
    Set<Contract> contracts;
}
