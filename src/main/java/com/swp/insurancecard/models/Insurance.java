package com.swp.insurancecard.models;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "insurances")
@Data
public class Insurance {
    @Id
    @Column(name = "insurance_id")
    Integer id;
    String name;
    String target;
    String shortDetail;
    String scope;
    String img;

    @OneToMany(mappedBy = "insurance", cascade = CascadeType.ALL)
    Set<Contract> contractSet;


}
