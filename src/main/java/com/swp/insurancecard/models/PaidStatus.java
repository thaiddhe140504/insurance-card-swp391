package com.swp.insurancecard.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Data
@Entity(name = "paid_status")
public class PaidStatus {
    @Id
    Integer id;
    String name;
    @OneToOne(mappedBy = "paidStatus",fetch = FetchType.LAZY)
    Punishment punishment;
}
