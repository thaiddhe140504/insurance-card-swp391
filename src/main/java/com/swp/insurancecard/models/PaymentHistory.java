package com.swp.insurancecard.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "payment_histories")
public class PaymentHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "paymenthistory_id")
    Integer id;
    String method;
    @Column(name = "contract_id")
    Integer contractId;
    long amount;
    String paidTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "contract_id",updatable = false, insertable = false)
    Contract contract;

    public String returnPrice(){
        String result="";
        long temp = this.amount;
        while(temp>0){
            result=(temp%1000==0?"000":temp%1000+"")+","+result;
            temp/=1000;
        }
        return result.substring(0,result.length()-1);
    }
}
