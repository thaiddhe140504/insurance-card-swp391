package com.swp.insurancecard.models;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "prices")
public class Price {
    @Id
    @Column(name = "price_id")
    Integer id;
    int price;

    @OneToMany(mappedBy = "price", cascade = CascadeType.ALL)
    Set<Contract> contracts;

    public String returnPrice(){
        String result="";
        int temp = this.price;
        while(temp>0){
            result=(temp%1000==0?"000":temp%1000+"")+","+result;
            temp/=1000;
        }
        return result.substring(0,result.length()-1);
    }
}
