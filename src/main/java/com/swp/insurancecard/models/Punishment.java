package com.swp.insurancecard.models;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "punishments")
public class Punishment {

    @Id
    @Column(name = "punishment_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;


    long amount;

    @Column(name = "contract_id")
    Integer contractId;

    @ManyToOne
    @JoinColumn(name = "status_id")
    Status status;

    String resolveTime;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "paid_status_id")
    PaidStatus paidStatus;

    String createTime;

    @ManyToOne
    @JoinColumn(name = "contract_id", updatable = false, insertable = false)
    Contract contract;

    public String returnPrice(){
        String result="";
        long temp = this.amount;
        while(temp>0){
            result=(temp%1000==0?"000":temp%1000+"")+","+result;
            temp/=1000;
        }
        return result.substring(0,result.length()-1);
    }
}
