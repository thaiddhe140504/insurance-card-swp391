package com.swp.insurancecard.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "request_compensation")
public class RequestCompensation {
    @Id
    @Column(name = "contract_id")
    Integer id;

    @ManyToOne
    @JoinColumn(name = "status_id")
    Status status;

    @OneToOne(mappedBy = "requestCompensation", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    Contract contract;

    long amount;

    String requestTime;
    String resolveTime;

    public String returnPrice(){
        String result="";
        long temp = this.amount;
        while(temp>0){
            result=(temp%1000==0?"000":temp%1000+"")+","+result;
            temp/=1000;
        }
        return result.substring(0,result.length()-1);
    }
}
