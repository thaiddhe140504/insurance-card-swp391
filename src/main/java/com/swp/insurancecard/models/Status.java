package com.swp.insurancecard.models;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity(name = "status")
public class Status {

    @Id
    @Column(name = "status_id")
    Integer id;

    String name;

    @OneToMany(cascade = CascadeType.ALL)
    Set<Accident> accident;

    @OneToMany(cascade = CascadeType.ALL)
    Set<Punishment> punishment;

    @OneToMany(cascade = CascadeType.ALL)
    Set<RequestCompensation> RequestCompensation;

}
