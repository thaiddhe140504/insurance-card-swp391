package com.swp.insurancecard.models;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@Entity(name = "users")
public class User {
    @Id
    @Column(name = "account_id")
    Long id;
    String gender;
    String phoneNumber;
    String address;
    String firstName;
    String middleName;
    String lastName;
    String DOB;
    String email;

    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private Account account;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    Set<Contract> contracts;

    Set<Contract> getContracts(){
        return this.contracts;
    }

    void setContracts(Set<Contract> contracts){
        this.contracts = contracts;
    }

    public User(Long id) {
        this.id = id;
    }
}
