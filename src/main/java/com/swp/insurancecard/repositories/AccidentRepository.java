package com.swp.insurancecard.repositories;

import com.swp.insurancecard.models.Accident;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AccidentRepository extends JpaRepository<Accident, Integer> {
    @Query("select a from accidents a where a.contract.user.id = :account_id")
    List<Accident> findAllByAccountId(@Param("account_id") Long account_id);
}
