package com.swp.insurancecard.repositories;

import com.swp.insurancecard.models.RequestCompensation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CompensationRepository extends JpaRepository<RequestCompensation, Integer> {
    @Query("select c from request_compensation c where c.contract.user.id = :account_id")
    List<RequestCompensation> findAllByAccountId(@Param("account_id")Long account_id);
}
