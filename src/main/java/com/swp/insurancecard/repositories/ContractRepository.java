package com.swp.insurancecard.repositories;

import com.swp.insurancecard.models.Contract;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ContractRepository extends JpaRepository<Contract, Integer> {
    @Query("select c from contracts c where c.user.id = :accountId")
    public List<Contract> findbyAccountId(@Param("accountId")Long accountId);

    @Query("select c from contracts c where c.user.id = :accountId and c.contractStatus.id = :statusId")
    public List<Contract> findbyAccountIdAndStatus(@Param("accountId")Long accountId, @Param("statusId")Integer statusId);

    @Query("select c from contracts c where c.contractStatus.id <> :statusId and c.contractStatus.id <> :statusId1")
    public List<Contract> findbyWithoutStatuses(@Param("statusId")Integer statusId, @Param("statusId1")Integer statusId1);

    @Query("select c from contracts c where c.contractStatus.id = :statusId or c.contractStatus.id = :statusId1")
    public List<Contract> findbyStatuses(@Param("statusId")Integer statusId, @Param("statusId1")Integer statusId1);

    @Query("select c from contracts c where c.contractStatus.id = :statusId ")
    public List<Contract> findbyStatus(@Param("statusId")Integer statusId);
}
