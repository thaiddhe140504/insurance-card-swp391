package com.swp.insurancecard.repositories;

import com.swp.insurancecard.models.ContractStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContractStatusRepository extends JpaRepository<ContractStatus, Integer> {
}
