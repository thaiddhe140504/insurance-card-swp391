package com.swp.insurancecard.repositories;

import com.swp.insurancecard.models.PaidStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaidStatusRepository extends JpaRepository<PaidStatus, Integer> {
}
