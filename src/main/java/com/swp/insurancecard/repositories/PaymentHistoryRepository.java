package com.swp.insurancecard.repositories;

import com.swp.insurancecard.models.PaymentHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PaymentHistoryRepository extends JpaRepository<PaymentHistory, Integer> {
    @Query("select p from payment_histories p where p.contract.user.account.id = :accountId")
    List<PaymentHistory> findAllByAccountId(@Param("accountId")Long accountId);
}
