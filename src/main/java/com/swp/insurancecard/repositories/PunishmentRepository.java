package com.swp.insurancecard.repositories;

import com.swp.insurancecard.models.Punishment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PunishmentRepository extends JpaRepository<Punishment, Integer> {
    @Query("select p from punishments p where p.contract.user.id = :account_id")
    List<Punishment> findAllByAccountId(@Param("account_id") Long id);
}
