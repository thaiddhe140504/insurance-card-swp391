package com.swp.insurancecard.repositories;

import com.swp.insurancecard.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer> {
}
