package com.swp.insurancecard.repositories;

import com.swp.insurancecard.models.Status;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusRepository extends JpaRepository<Status, Integer> {
}
