package com.swp.insurancecard.repositories;

import com.swp.insurancecard.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
