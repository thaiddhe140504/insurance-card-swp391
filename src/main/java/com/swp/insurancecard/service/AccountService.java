package com.swp.insurancecard.service;

import com.swp.insurancecard.detail.AccountDetail;
import com.swp.insurancecard.models.Account;
import com.swp.insurancecard.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class AccountService implements UserDetailsService {
    @Autowired
    private AccountRepository accountRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Account account = accountRepository.getAccountByUserName(username);
        if(account == null){
            throw new UsernameNotFoundException("Không tìm thấy user");
        }
        return new AccountDetail(account);
    }

    public void saveAccount(Account account){
        accountRepository.save(account);
    }

}
